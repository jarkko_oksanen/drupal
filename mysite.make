core = 7.x
api = 2



; Defaults

defaults[projects][type] = module
defaults[projects][subdir] = contrib


; Modules
projects[admin_menu][version] = 3.0-rc5

projects[floating_block][version] = 1.2

projects[ctools][version] = 1.6

projects[devel][version] = 1.5

projects[diff][version] = 3.2

projects[features][version] = 2.4

projects[views][version] = 3.10

projects[getlocations][version] = 1.1

