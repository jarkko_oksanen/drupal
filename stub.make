core = 7.x
api = 2



; Drupal core

projects[drupal][type] = core
projects[drupal][version] = 7.34



; Install profile

projects[mysite][type] = profile
projects[mysite][download][type] = git
projects[mysite][download][url] = git@bitbucket.org:jarkko_oksanen/drupal.git
projects[mysite][download][branch] = master
